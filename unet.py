from datetime import datetime
import os
import random

import tensorflow as tf
from tensorflow.python.framework.errors import ResourceExhaustedError
import numpy as np

# tf.logging.set_verbosity(tf.logging.ERROR)

tf.summary.FileWriterCache.clear()


class UNet:
    def __init__(self, layers, categories, features, size=(240, 320), slices=2):
        self.sess = None

        self.writer = tf.summary.FileWriter('unet_board')

        self.slices = slices
        self.categories = categories

        self.flip = tf.placeholder("bool", name="flip")
        self.image_name = tf.placeholder("string", name="image_name")

        self.x, self.y = self.load_image(self.image_name, size)

        self.logits = self.create_network(self.x, layers=layers, features=features)
        self.loss = self.loss_fun(categories, size)
        self.accuracy = self.accuracy_fun()
        self.train_step = tf.train.AdamOptimizer().minimize(self.loss)

        self.saver = tf.train.Saver()

    def load_image(self, image_name, size):
        image = tf.image.decode_jpeg(tf.read_file(prefix + 'images/' + image_name + '.jpg'), 3)
        image = tf.expand_dims(image, axis=0)

        self.image = image

        image = tf.image.resize_bilinear(image, [size[0] * self.slices, size[1] * self.slices])
        image = tf.cond(self.flip, lambda: tf.image.flip_left_right(image), lambda: image)
        image = tf.image.convert_image_dtype(image, tf.float32)

        labels = tf.image.decode_png(tf.read_file(prefix + 'labels_plain/' + image_name + '.png'), 1)
        labels = tf.expand_dims(labels, axis=0)
        labels = tf.cond(self.flip, lambda: tf.image.flip_left_right(labels), lambda: labels)
        labels = tf.one_hot(tf.squeeze(labels), depth=self.categories)
        labels = tf.expand_dims(labels, axis=0)

        image = tf.convert_to_tensor(tf.split(image, num_or_size_splits=self.slices, axis=2))
        image = tf.convert_to_tensor(tf.split(image, num_or_size_splits=self.slices, axis=2))
        image = tf.reshape(image, [self.slices ** 2, size[0], size[1], 3])

        return image, labels

    def create_network(self, signal, layers, features, kernel=(3, 3)):
        copy_crop = []

        for _ in range(layers):
            signal = self.conv(signal, features, kernel)
            signal = self.conv(signal, features, kernel)
            copy_crop.append(signal)

            signal = self.pool(signal)
            features *= 2

        signal = self.conv(signal, features, kernel)

        for i in reversed(range(layers)):
            features //= 2
            signal = self.conv(signal, features, kernel)

            signal = self.up_conv(signal, features, [2, 2])
            signal = self.concat(signal, copy_crop[i])

            signal = self.conv(signal, features, kernel)

        signal = self.conv(signal, features, kernel)
        signal = self.conv(signal, features, kernel)

        return self.conv(signal, self.categories, [1, 1], activation=None)

    def conv(self, signal, features, kernel, activation=tf.nn.relu):
        signal = tf.layers.batch_normalization(signal, axis=3)
        signal = tf.layers.conv2d(inputs=signal, filters=features, kernel_size=kernel,
                                  padding="same", activation=activation)

        return signal

    def pool(self, signal):
        return tf.layers.max_pooling2d(inputs=signal, pool_size=2, strides=2)

    def up_conv(self, signal, features, kernel):
        return tf.layers.conv2d_transpose(signal, features, kernel, strides=2, padding='same',
                                          kernel_initializer=tf.initializers.ones)

    def concat(self, signal1, signal2):
        return tf.concat([signal1, signal2], axis=3)

    def loss_fun(self, categories, size):
        self.logits = tf.reshape(self.logits, [self.slices, self.slices, size[0], size[1], categories])
        self.logits = tf.concat([self.logits[i] for i in range(self.slices)], axis=1)
        self.logits = tf.concat([self.logits[i] for i in range(self.slices)], axis=1)
        self.logits = tf.expand_dims(self.logits, axis=0)

        image_shape = tf.shape(self.y)

        training_logits = tf.image.resize_nearest_neighbor(self.logits, [image_shape[1]//2, image_shape[2]//2])
        training_labels = tf.image.resize_nearest_neighbor(self.y, [image_shape[1]//2, image_shape[2]//2])

        cross_entropy = tf.losses.softmax_cross_entropy(training_labels, training_logits)

        return tf.reduce_mean(cross_entropy)

    def accuracy_fun(self):
        image_shape = tf.shape(self.y)
        self.logits = tf.image.resize_nearest_neighbor(self.logits, [image_shape[1], image_shape[2]])

        return tf.reduce_mean(tf.cast(tf.equal(tf.argmax(self.logits, axis=3),
                                               tf.argmax(self.y, axis=3)), tf.float32))

    def train(self, batches, image_names, test_names, info, save=None):
        with tf.Session() as self.sess:
            tf.global_variables_initializer().run()

            losses, accs = [], []
            try:
                for i in range(1, batches+1):
                    loss, acc = self.train_on_image(random.choice(image_names))
                    losses.append(loss)
                    accs.append(acc)

                    if i % info == 0:
                        print('Iter {} {}'.format(i, datetime.utcnow()), flush=True)
                        print('Loss {}: Accuracy {}'.format(np.mean(losses[-info:]),
                                                            np.mean(accs[-info:])), flush=True)

            except KeyboardInterrupt:
                pass

            print('End training {}'.format(datetime.utcnow()), flush=True)

            if save:
                self.saver.save(self.sess, save)

            info /= 10

            test_accuracies = []
            for i, image_name in enumerate(test_names):
                test_accuracies.append(self.categorise_image(i, image_name))

                if (i + 1) % info == 0:
                    print('Test {} {}'.format((i+1), datetime.utcnow()), flush=True)

            return test_accuracies

    def restore_test(self, test_names, info, restore):
        with tf.Session() as self.sess:
            self.saver.restore(self.sess, restore)

            test_accuracies = []
            for i, image_name in enumerate(test_names):
                test_accuracies.append(self.categorise_image(i, image_name))

                if (i + 1) % info == 0:
                    print('Test {} {}'.format((i+1), datetime.utcnow()), flush=True)

            return test_accuracies

    def train_on_image(self, image_name):
        try:
            _, loss, acc = self.sess.run([self.train_step, self.loss, self.accuracy],
                                         feed_dict={self.image_name: image_name,
                                                    self.flip: random.choice([True, False])})
        except ResourceExhaustedError:
            print('ResourseExhaustedError occured', flush=True)
            return 2, 0
        return loss, acc

    def categorise_image(self, index, image_name):
        try:
            feed_dict = {
                self.image_name: image_name,
                self.flip: False,
            }
            acc = self.sess.run(self.accuracy, feed_dict=feed_dict)
            board_predict = self.sess.run(tf.summary.image(
                'Test_{}_acc={}%'.format(image_name, acc * 100.),
                tf.concat([
                    self.image,
                    tf.image.grayscale_to_rgb(tf.cast(tf.expand_dims(tf.argmax(self.y, 3), axis=3), tf.uint8)),
                    tf.image.grayscale_to_rgb(tf.cast(tf.expand_dims(tf.argmax(self.logits, 3), axis=3), tf.uint8)),
                ], axis=0)), feed_dict=feed_dict)

            self.writer.add_summary(board_predict, index)

            feed_dict_flip = {
                self.image_name: image_name,
                self.flip: True,
            }
            acc_flip = self.sess.run(self.accuracy, feed_dict=feed_dict_flip)
            board_predict_flip = self.sess.run(tf.summary.image(
                'Test_{}_flip_acc={}%'.format(image_name, acc_flip * 100.),
                tf.concat([
                    tf.image.flip_left_right(self.image),
                    tf.image.grayscale_to_rgb(tf.cast(tf.expand_dims(tf.argmax(self.y, 3), axis=3), tf.uint8)),
                    tf.image.grayscale_to_rgb(tf.cast(tf.expand_dims(tf.argmax(self.logits, 3), axis=3), tf.uint8)),
                ], axis=0)), feed_dict=feed_dict_flip)

            self.writer.add_summary(board_predict_flip, index)
        except ResourceExhaustedError:
            print('ResourseExhaustedError occured (should not happen ever)', flush=True)
            return 0
        return (acc + acc_flip) / 2.


prefix = '/scidata/assignment2/training/'
# prefix = 'assignment2/training/'
save = 'test'


def get_files():
    return list(map(lambda x: x.split('.')[0], os.listdir(prefix + 'images/')))


if __name__ == '__main__':
    files = get_files()
    random.shuffle(files)
    unet = UNet(categories=66, features=64, layers=4, size=(256, 256), slices=2)
    accuracies = unet.train(40000, files[:10000], files[10000:10100], info=100, save='./unet_model_40k.ckpt')
    # accuracies = unet.restore_test(files[10:11], info=1, restore='./unet_model_50k.ckpt')
    print('End testing {}'.format(datetime.utcnow()), flush=True)
    print('Final accuracy : {}'.format(np.mean(accuracies)), flush=True)
