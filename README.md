## Unet

UNet fully convolutional network for semantic segmentation of real-life photos.


#### Features

Unet used for semantic segmentation of real-life photos with 66 classification classes.
Solution utilizes slicing and flipping of images to generate more examples for feature augmentation.

#### Results

This implementation achieved ~70% of accuracy after few hours of learning on high-performance GPU.
In hindsight, slicing of images generated poor source data, which may have lowered the quality of model 
(colleagues achieved even ~80% in shorter time).

![alt text](screenshot.png)
